// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "BonusBase.generated.h"

UENUM(BlueprintType)
enum class EBonusType : uint8
{
	NO_BONUS	UMETA(DisplayName = "No Bonus"),
	MORE_FOOD	UMETA(DisplayName = "More Food"),
	IMMORTAL	UMETA(DisplayName = "Temporary Immortal")
};

UCLASS()
class SNAKEGAME_API ABonusBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusBase();

	UPROPERTY(BlueprintReadOnly)
		EBonusType BonusType;
	UPROPERTY(EditDefaultsOnly)
		float BonusLivesTime = 15.f;
	UPROPERTY(EditDefaultsOnly)
		float BonusSpeed = 0.1f;
	UPROPERTY(EditDefaultsOnly)
		int32 BonusFoodCount = 2;
	UPROPERTY(EditDefaultsOnly)
		int32 ImmortalDuration = 10;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void LivesTimeHasEnded();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

private:
	FTimerHandle TimerHandler;

private:
	EBonusType GetRandBonus();
};
