// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedUpBonusBase.h"
#include "SnakeBase.h"

// Sets default values
ASpeedUpBonusBase::ASpeedUpBonusBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpeedBonusValue = 0.5;

}

// Called when the game starts or when spawned
void ASpeedUpBonusBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedUpBonusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedUpBonusBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->SpeedUp(SpeedBonusValue);
		this->Destroy();
	}
}

