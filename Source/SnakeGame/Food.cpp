// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "SnakeGameGameModeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AFood::Destroyed()
{
	auto GameMode = GetWorld()->GetAuthGameMode<ASnakeGameGameModeBase>();
	if (IsValid(GameMode))
	{
		--GameMode->SpawnedFoodCount;
	}
	Super::Destroyed();
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(LivesTimerHandler, this, &AFood::LivesTimeHasEnded, FoodLivesTime, false);
}

void AFood::LivesTimeHasEnded()
{
	auto GameMode = GetWorld()->GetAuthGameMode<ASnakeGameGameModeBase>();
	if (IsValid(GameMode))
	{
		if (GameMode->SpawnedFoodCount > 1)
		{
			//GameMode->SpawnedFoodCount--;
			this->Destroy();
		}
	}

}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			ASnakeGameGameModeBase* GameMode = Cast<ASnakeGameGameModeBase>(GetWorld()->GetAuthGameMode());
			if (IsValid(GameMode)) {
				GameMode->FoodHasEated();
				GameMode->AddFood();
			}
			this->Destroy();
		}
	}
}

