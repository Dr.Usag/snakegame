// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBase.h"
#include "SnakeGameGameModeBase.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
ABonusBase::ABonusBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABonusBase::BeginPlay()
{
	BonusType = GetRandBonus();

	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(TimerHandler, this, &ABonusBase::LivesTimeHasEnded, BonusLivesTime, false);
}

void ABonusBase::LivesTimeHasEnded()
{
	this->Destroy();
}


// Called every frame
void ABonusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto GameMode = GetWorld()->GetAuthGameMode<ASnakeGameGameModeBase>();
	if (IsValid(GameMode))
	{
		switch (BonusType)
		{
		case EBonusType::MORE_FOOD:
			GameMode->AddFood(BonusFoodCount);
			break;
		case EBonusType::IMMORTAL:
			GameMode->AddSnakeImmortal(ImmortalDuration);
		}
		this->Destroy();
	}

}

EBonusType ABonusBase::GetRandBonus()
{
	EBonusType Type = EBonusType::NO_BONUS;

	auto GameMode = GetWorld()->GetAuthGameMode<ASnakeGameGameModeBase>();
	if (IsValid(GameMode))
	{
		auto Probabilities = GameMode->GetBonusProbabilitites();
		float RandProbability = FMath::RandRange(0.f, GameMode->GetBonusProbabilitesSum());
		for (const auto& Item : Probabilities)
		{
			RandProbability -= Item.Value;
			if (RandProbability < 0)
			{
				Type = static_cast<EBonusType>(Item.Key);
				break;
			}
		}
	}
	return Type;
}
