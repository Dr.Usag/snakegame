// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameGameModeBase.generated.h"

class AFood;
class ASnakeBase;
class ABonusBase;

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASnakeBase* SnakeActor = nullptr;
	int32 SpawnedFoodCount;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonusBase> BonusClass;

	UPROPERTY(EditDefaultsOnly)
		float SpawnBonusRate = 10.f;
	UPROPERTY(EditDefaultsOnly)
		int32 SpeedUpPerFoodRate = 5;
	UPROPERTY(EditDefaultsOnly)
		float SpeedUpRate = 0.02f;
	UPROPERTY(EditDefaultsOnly)
		float SpawnBonusProbability = 0.3f;

	// Bonus Types Probabilities
	UPROPERTY(EditDefaultsOnly)
		float FoodBonusProbabilities = 0.f;
	UPROPERTY(EditDefaultsOnly)
		float ImmortalBonusProbabilities = 0.f;

public:
	ASnakeGameGameModeBase();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void AddFood(int32 count = 1);
	UFUNCTION()
		void AddBonus();
	UFUNCTION()
		void AddSnakeSpeed(float speed);
	UFUNCTION()
		void AddSnakeImmortal(int32 duration);
	UFUNCTION()
		void FoodHasEated();
	UFUNCTION()
		void SnakeHasDead();

	float GetBonusProbabilitesSum() const;
	const TMap<int32, float>& GetBonusProbabilitites() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/* Handle to manage the timer */
	FTimerHandle TimerHandler;

	// TODO: Change to scores system!
	int32 EatedFood = 0;
	// TODO: END

	/* * * * * * * * * * * * * * * * * * *
	 * Spawn collisions sphere parameters
	 * * * * * * * * * * * * * * * * * * */

	TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
	// Ignore any specific actors
	TArray<AActor*> IgnoreActors;
	// Array of actors that are inside the radius of the sphere
	TArray<AActor*> OutActors;
	// Class that the sphere should hit against and include in the outActors array (Can be null)
	UClass* SeekClass = nullptr;
	float SphereRadius = 100.f;

private:
	FTransform RandTransform();
	FVector RandLocation();

	TMap<int32, float> BonusProbabilites;
	float BonusProbabilitesSum = 0.f;
};
