// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameGameModeBase.h"
#include "Food.h"
#include "BonusBase.h"
#include "SnakeBase.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetSystemLibrary.h"

ASnakeGameGameModeBase::ASnakeGameGameModeBase()
{
	TraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_PhysicsBody));
	// Ignore self or remove this line to not ignore any
	//ignoreActors.Init(this, 1);
}

void ASnakeGameGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASnakeGameGameModeBase::AddFood(int32 count)
{
	for (int32 i = 0; i < count; ++i)
	{
		GetWorld()->SpawnActor<AFood>(FoodClass, RandTransform());
		SpawnedFoodCount++;
	}
}

void ASnakeGameGameModeBase::AddBonus()
{	
	float SpawnProbability = FMath::RandRange(0.f, 1.f);
	if (SpawnProbability <= SpawnBonusProbability && BonusProbabilitesSum > 0.f)
	{
		GetWorld()->SpawnActor<ABonusBase>(BonusClass, RandTransform());
	}
}

void ASnakeGameGameModeBase::AddSnakeSpeed(float speed)
{
	if (IsValid(SnakeActor))
	{
		SnakeActor->SpeedUp(speed);
	}
}

void ASnakeGameGameModeBase::AddSnakeImmortal(int32 duration)
{
	if (IsValid(SnakeActor))
	{
		SnakeActor->SetSnakeImmortal(duration);
	}
}

void ASnakeGameGameModeBase::FoodHasEated()
{
	if (IsValid(SnakeActor))
	{
		++EatedFood;
		//--SpawnedFoodCount;
		if (EatedFood >= SpeedUpPerFoodRate && EatedFood % SpeedUpPerFoodRate == 0)
		{
			SnakeActor->SpeedUp(SpeedUpRate);
		}
	}
}

void ASnakeGameGameModeBase::SnakeHasDead()
{
	GetWorldTimerManager().ClearAllTimersForObject(this);
}

float ASnakeGameGameModeBase::GetBonusProbabilitesSum() const
{
	return BonusProbabilitesSum;
}

const TMap<int32, float>& ASnakeGameGameModeBase::GetBonusProbabilitites() const
{
	return BonusProbabilites;
}

void ASnakeGameGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	BonusProbabilites =
	{
		{ static_cast<int32>(EBonusType::MORE_FOOD), FoodBonusProbabilities    },
		{ static_cast<int32>(EBonusType::IMMORTAL), ImmortalBonusProbabilities }
	};

	for (const auto& Item : BonusProbabilites)
	{
		BonusProbabilitesSum += Item.Value;
	}

	this->AddFood();
	GetWorldTimerManager().SetTimer(TimerHandler, this, &ASnakeGameGameModeBase::AddBonus, SpawnBonusRate, true, 1.f);
}

FTransform ASnakeGameGameModeBase::RandTransform()
{
	FVector NewLocation(ForceInitToZero);
	bool bIsOverlapped = true;
	do 
	{
		NewLocation = RandLocation();
		bIsOverlapped = UKismetSystemLibrary::SphereOverlapActors
		(
			GetWorld(),
			NewLocation,
			SphereRadius,
			TraceObjectTypes,
			SeekClass,
			IgnoreActors,
			OutActors
		);
		//UKismetSystemLibrary::DrawDebugSphere(GetWorld(), NewLocation, radius, 12, FColor::Red, true, 10.0f);
	} while (bIsOverlapped);

	return FTransform(NewLocation);
}

FVector ASnakeGameGameModeBase::RandLocation()
{
	float X = FMath::RandRange(-600.f, 600.f);
	float Y = FMath::RandRange(-600.f, 600.f);
	float Z = 0.f;

	return FVector(X,Y,Z);
}
