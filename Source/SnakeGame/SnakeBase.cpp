// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "SnakeGameGameModeBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10;
	TickIntervalLimit = 0.1f;
	LastMoveDirection = EMovementDirection::DOWN;
	IsSnakeMovingCompleted = true;

}

void ASnakeBase::Destroyed()
{
	auto GameMode = GetWorld()->GetAuthGameMode<ASnakeGameGameModeBase>();
	if (IsValid(GameMode))
	{
		GameMode->SnakeHasDead();
	}
	Super::Destroyed();
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();	
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::ToggleImmortal_Implementation()
{
	bIsImmortal = bIsImmortal ? false : true;
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) {
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		//NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		NewSnakeElem->SnakeOwner = this;

		int32 ElementIndex = SnakeElements.Add(NewSnakeElem);
		if (ElementIndex == 0) {
			NewSnakeElem->SetFirstElementType();
			NewSnakeElem->MeshComponent->SetVisibility(true);
			NewSnakeElem->ToggleCollision();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurrentElement  = SnakeElements[i];
		auto PrevElement     = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();

		CurrentElement->SetActorLocation(PrevLocation);
		if (CurrentElement->bIsNewElement) 
		{
			CurrentElement->bIsNewElement = false;
			CurrentElement->MeshComponent->SetVisibility(true);
			CurrentElement->ToggleCollision();
		}
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector); // SetActorLocation(GetActorLocation()+MovementVector
	SnakeElements[0]->ToggleCollision();

	IsSnakeMovingCompleted = true;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SpeedUp(float value)
{
	float NewMovementSpeed = MovementSpeed - value;
	if (NewMovementSpeed > TickIntervalLimit)
	{
		MovementSpeed = NewMovementSpeed;
		SetActorTickInterval(MovementSpeed);

	}
}

void ASnakeBase::SetSnakeImmortal(float Duration)
{
	if (!bIsImmortal)
	{
		this->ToggleImmortal();
		GetWorldTimerManager().SetTimer(ImmortalTimeHandler, this, &ASnakeBase::ToggleImmortal, Duration, false);
	}

}

bool ASnakeBase::GetImmortalState() const
{
	return bIsImmortal;
}

